#!/usr/bin/env bash

if [ $# -eq 0 ];  then
    echo "Usage : check_onnx_model.sh model_file.onnx [trtexec options like --fp16 or --workspace=2000...]"
    exit 1
fi

MODEL_PATH="$1"
# By default mount the current folder
VOLUME_HOST="$(pwd)"
VOLUME_DOCKER="/host_volume"
TRTEXEC_ARGS="--onnx=$VOLUME_DOCKER/$MODEL_PATH"

# JP 3.3 : TRT 4.0
# JP 4.1.1 : TRT 5.0
# JP 4.2.0 : TRT 5.0
# JP 4.2.1 : TRT 5.1
# JP 4.3 : TRT 6.0

#trt_versions=( "4.0" "5.0" "5.1" "6.0" "7.0" )
trt_versions=( "5.1" "6.0" "7.0" )
 echo -e "Testing TensorRT..."

for version in "${trt_versions[@]}"; do
    #echo -e "\n\n==================== TensorRT ${version} ===================\n"
    echo -e " ${version}"
    #docker run --gpus all -v "$VOLUME_HOST:$VOLUME_DOCKER" adujardin/tensorrt-trtexec:$version $TRTEXEC_ARGS | tee "log_trt${version}_${MODEL_PATH}.txt"
    docker run --gpus all -v "$VOLUME_HOST:$VOLUME_DOCKER":ro adujardin/tensorrt-trtexec:$version $TRTEXEC_ARGS &> "log_trt${version}_${MODEL_PATH}.txt"
done

echo -e "\n=====================================================\n"

fail_str="&&&& FAILED TensorRT.trtexec"
ok_str="&&&& PASSED TensorRT.trtexec"
log_file="log_${MODEL_PATH}.txt"
raw_log_file="rawlog_${MODEL_PATH}.txt"

for version in "${trt_versions[@]}"; do
    in_log_file="log_trt${version}_${MODEL_PATH}.txt"
    echo -e "\n\n==================== TensorRT ${version} ===================\n" >> "${log_file}"
    echo -e "\n\n==================== TensorRT ${version} ===================\n" >> "${raw_log_file}"
    tail -n 20 "${in_log_file}" >> "${log_file}"
    cat "${in_log_file}" >> "${raw_log_file}"

    echo -en "TensorRT ${version}"
    if grep -q "${fail_str}" "${in_log_file}"; then
        printf "\033[31m FAILED \033[39m\n"
    else
        printf "\033[32m PASSED \033[39m\n"
    fi

    rm "${in_log_file}"
done

# Verbose
#cat "log_${MODEL_PATH}.txt"
echo -e "\nFor more information :\n  cat ${log_file}\n"