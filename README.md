# TensorRT Docker

The goal of this project is to test model against all version of tensorRT easily to assess compatiblity.

## Will it run with TRT X.Y ?

The test script is still quite basic, and requires the model to be put in the current folder.

Example :

```sh
./check_onnx_model.sh model.onnx --verbose --fp16 --workspace=2000
Testing TensorRT...
 5.1
 6.0
 7.0

=====================================================

TensorRT 5.1 FAILED 
TensorRT 6.0 PASSED 
TensorRT 7.0 PASSED 

For more information :
  cat log_model.onnx.txt

```

By default the script will check against TRT 5.1 -> 7.0 ( = JetPack 4.2.1 and above).

The logs are displayed in real time and stored into log files (like "log_trt5.1_model.onnx.txt"). Since this is the stock version of trtexec, the logging isn't consistent between version and quite verbose unfortunelty. It's currently not possible to have a output like "TRT 6.0 : Compatible, took N ms (median, end to end)".


## To dev this tool

### Docker

It uses a docker image with a compatible OS/Cuda version for each TensorRT, see the dockerfiles in each folders.

This project is built by a Gitlab CI https://gitlab.com/adujardin/tensorrt-docker/ and mirrored on Github https://github.com/adujardin/tensorrt-docker

### TensorRT repacking

To generate the tar gz currently stored, a script is used to stripped down the original tar gz of any unwanted stuff.

Usage :

Put all TensorRT installer in a folder, let's say `trt_installers` :

```sh
du -hs trt_installers/*
    392M	trt_installers/TensorRT-4.0.1.6.Ubuntu-16.04.4.x86_64-gnu.cuda-9.0.cudnn7.1.tar.gz
    589M	trt_installers/TensorRT-5.0.2.6.Ubuntu-18.04.1.x86_64-gnu.cuda-10.0.cudnn7.3.tar.gz
    603M	trt_installers/TensorRT-5.1.5.0.Ubuntu-18.04.2.x86_64-gnu.cuda-10.0.cudnn7.5.tar.gz
    682M	trt_installers/TensorRT-6.0.1.5.Ubuntu-18.04.x86_64-gnu.cuda-10.0.cudnn7.6.tar.gz
    710M	trt_installers/TensorRT-7.0.0.11.Ubuntu-18.04.x86_64-gnu.cuda-10.0.cudnn7.6.tar.gz

# repack_tensorRT is waiting for an absolute path
bash utils/repack_tensorRT.sh /foo/trt_installers
# Some errors might come up, nothing serious

# Then :
du -hs trt_installers/repack/*
    50M	    trt_all/repack/TensorRT-4.0.1.6.Ubuntu-16.04.4.x86_64-gnu.cuda-9.0.cudnn7.1.tar.gz
    57M	    trt_all/repack/TensorRT-5.0.2.6.Ubuntu-18.04.1.x86_64-gnu.cuda-10.0.cudnn7.3.tar.gz
    65M	    trt_all/repack/TensorRT-5.1.5.0.Ubuntu-18.04.2.x86_64-gnu.cuda-10.0.cudnn7.5.tar.gz
    113M	trt_all/repack/TensorRT-6.0.1.5.Ubuntu-18.04.x86_64-gnu.cuda-10.0.cudnn7.6.tar.gz
    118M	trt_all/repack/TensorRT-7.0.0.11.Ubuntu-18.04.x86_64-gnu.cuda-10.0.cudnn7.6.tar.gz

# Much better !
```

It uses Git LFS to store the TensorRT libraries. This repo is therefore quite heavy, but since the docker images are all available on DockerHub, only the [script](https://github.com/adujardin/tensorrt-docker/blob/master/check_onnx_model.sh) is actually needed.
